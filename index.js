const DSNV = "DSNV";
var staffList = [];
function luuLocalStorage() {
  let jsonStaffList = JSON.stringify(staffList);
  // chỉ lưu được kiểu dữ liệu jason nên phải chuyển dữ liệu sang json
  localStorage.setItem(DSNV, jsonStaffList);
}
// lấy dữ liệu từ localStorage khi user load trang
// localStorage.getItem: lấy dữ liệu từ local
var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
  // convert từ json thành array
  var nvArr = JSON.parse(dataJson);
  // convert data vì dữ liệu không có method tinhDTB
  for (let index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new staffObj(
      item.user,
      item.name,
      item.email,
      item.pass,
      item.workDays,
      item.baseSal,
      item.position,
      item.workHours
    );
    staffList.push(nv);
  }
  renderStaffList(staffList);
}

function addStaff() {
  document.getElementById("tknv").disabled = false;
  var staff = getInfo();

  var isValidUser =
    kiemTraRong(staff.user, "tbTKNV", "Tài khoản không được để trống") &&
    kiemTraTrung(staff.user, staffList, "tbTKNV", "Tài khoản đã tồn tại") &&
    kiemTraSo(staff.user, "tbTKNV", "Vui lòng nhập tài khoản từ 4 - 6 chữ số");
  var isValid = validateData(staff);

  if (isValidUser && isValid) {
    staffList.push(staff);
    luuLocalStorage();
    renderStaffList(staffList);
    resetForm();
  }
}
console.log("staffList : ", staffList);

function delStaff(id) {
  var viTri = getIndex(id, staffList);
  if (viTri !== -1) {
    staffList.splice(viTri, 1);
    luuLocalStorage();

    // render lại giao diện sau khi xóa
    renderStaffList(staffList);
  }
}

function editStaff(id) {
  resetErr();
  var viTri = getIndex(id, staffList);
  if (viTri !== -1) {
    var data = staffList[viTri];
    reshowInfo(data);
    document.getElementById("tknv").disabled = true;
  }
}

function updateStaff() {
  var data = getInfo();
  var viTri = getIndex(data.user, staffList);
  var isValid = validateData(data);
  if (viTri !== -1 && isValid == true) {
    staffList[viTri] = data;
    renderStaffList(staffList);
    luuLocalStorage();
    document.getElementById("tknv").disabled = false;
    resetForm();
  }
}

const timNhanVienBtn = document.getElementById("btnTimNV");
timNhanVienBtn.addEventListener("click", function () {
  console.log("yes");
  document.getElementById("tbTimKiem").innerHTML = "";
  var staffListByClass = [];
  var searchInput = document.getElementById("searchName").value;

  if (searchInput !== "") {
    for (var index = 0; index < staffList.length; index++) {
      var itemNV = staffList[index];
      var loaiNhanVien = kiemTraTimKiem(searchInput, itemNV.classify());
      if (loaiNhanVien) {
        console.log("itemNV: ", itemNV);
        staffListByClass.push(itemNV);
      }
    }
    if (staffListByClass.length !== 0) {
      document.getElementById("tbTimKiem").innerHTML = `
      Tìm thấy ${staffListByClass.length} kết quả cho từ khóa "${searchInput}" `;
      renderStaffList(staffListByClass);
    } else {
      document.getElementById("tbTimKiem").innerHTML = `
      Không tìm thấy kết quả nào cho từ khóa "${searchInput}" `;
    }
  } else {
    renderStaffList(staffList);
  }
});
