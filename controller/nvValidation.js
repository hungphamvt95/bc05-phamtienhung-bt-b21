function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message, "initial");
    return false;
  } else {
    showMessageErr(idErr, "", "none");
    return true;
  }
}
function kiemTraTrung(userInput, staffList, idErr, message) {
  let index = getIndex(userInput, staffList);
  if (index !== -1) {
    showMessageErr(idErr, message, "initial");
    return false;
  } else {
    showMessageErr(idErr, "", "none");
    return true;
  }
}

function kiemTraSo(value, idErr, message) {
  var reg = /\d{4,6}/g;
  let isNumber = reg.test(value);
  if (isNumber) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function kiemTraChu(text, idErr, message) {
  var reg = /^[A-Za-z][A-Za-z\s]*$/;
  let isText = reg.test(text);
  if (isText) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function kiemTraEmail(value, idErr, message) {
  var reg =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let isEmail = reg.test(value);
  if (isEmail) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function kiemTraPass(value, idErr, message) {
  var reg = /(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
  let isPass = reg.test(value);
  if (isPass) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function kiemTraNgay(value, idErr, message) {
  var reg =
    /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19[0-9][0-9]|20[0-2][0-9])$/;
  let isDate = reg.test(value);
  if (isDate) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}
function kiemTraLuong(value, idErr, message) {
  if (value >= 1000000 && value <= 20000000) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function kiemTraSoGioLam(value, idErr, message) {
  if (value >= 80 && value <= 200) {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function kiemTraChucVu(value, idErr, message) {
  if (value == "Giám đốc" || value == "Trưởng phòng" || value == "Nhân viên") {
    showMessageErr(idErr, "", "none");
    return true;
  } else {
    showMessageErr(idErr, message, "initial");
    return false;
  }
}

function validateData(staffData) {
  var isValidName =
    kiemTraRong(staffData.name, "tbTen", "Tên không được để trống") &&
    kiemTraChu(staffData.name, "tbTen", "Tên phải là ký tự chữ");
  var isValidEmail =
    kiemTraRong(staffData.email, "tbEmail", "Email không được để trống") &&
    kiemTraEmail(staffData.email, "tbEmail", "Email chưa đúng định dạng");
  var isValidPass =
    kiemTraRong(staffData.pass, "tbMatKhau", "Mật khẩu không được để trống") &&
    kiemTraPass(
      staffData.pass,
      "tbMatKhau",
      "Mật khẩu phải từ 6-10 ký tự(chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt) "
    );
  var isValidDate =
    kiemTraRong(staffData.workDays, "tbNgay", "Ngày làm không được để trống") &&
    kiemTraNgay(staffData.workDays, "tbNgay", "Ngày làm định dạng mm/dd/yyyy");
  var isValidSalary =
    kiemTraRong(staffData.baseSal, "tbLuongCB", "Lương không được để trống") &&
    kiemTraLuong(
      staffData.baseSal,
      "tbLuongCB",
      "Lương cơ bản 1 000 000 - 20 000 000"
    );
  var isValidPosition = kiemTraChucVu(
    staffData.position,
    "tbChucVu",
    "Vui lòng chọn chức vụ"
  );
  var isValidWorkHours =
    kiemTraRong(
      staffData.workHours,
      "tbGiolam",
      "Giờ làm không được để trống"
    ) &&
    kiemTraSoGioLam(
      staffData.workHours,
      "tbGiolam",
      "Nhập số giờ làm trong tháng 80 - 200 giờ"
    );

  if (
    isValidName &&
    isValidEmail &&
    isValidPass &&
    isValidDate &&
    isValidSalary &&
    isValidPosition &&
    isValidWorkHours
  ) {
    return true;
  } else {
    return false;
  }
}

function kiemTraTimKiem(value, reg) {
  let isSearch = reg.includes(value);
  if (isSearch) {
    return true;
  } else {
    return false;
  }
}
