function getInfo() {
  var staffID = document.getElementById("tknv").value;
  var staffName = document.getElementById("name").value;
  var staffEmail = document.getElementById("email").value;
  var staffPass = document.getElementById("password").value;
  var workDays = document.getElementById("datepicker").value;
  var baseSal = document.getElementById("luongCB").value * 1;
  var position = document.getElementById("chucvu").value;
  var workHours = document.getElementById("gioLam").value * 1;

  var staff = new staffObj(
    staffID,
    staffName,
    staffEmail,
    staffPass,
    workDays,
    baseSal,
    position,
    workHours
  );
  return staff;
}

function renderStaffList(staffArr) {
  var contentHTML = "";
  for (var index = 0; index < staffArr.length; index++) {
    var currentStaff = staffArr[index];

    var contentTr = `
    <tr>
    <td>ID ${currentStaff.user}</td>
    <td>${currentStaff.name}</td>
    <td>${currentStaff.email}</td>
    <td>${currentStaff.workDays}</td>
    <td>${currentStaff.position}</td>
    <td>${currentStaff.totalSalCal()}</td>
    <td>${currentStaff.classify()}</td>
    <td>
    <button onclick="delStaff('${currentStaff.user}')" 
    class="btn btn-danger"><i class="fa fa-trash"></i></button>
    <button onclick="editStaff('${currentStaff.user}')"
    class="btn btn-warning" data-toggle = "modal" data-target = "#myModal"><i class="fa fa-edit"></i></button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  }
  console.log("currentStaff: ", currentStaff);
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function getIndex(id, staffArr) {
  for (var index = 0; index < staffArr.length; index++) {
    var item = staffArr[index];
    // nếu tìm thấy thì dừng function và trả về index hiện tại
    if (item.user == id) {
      return index;
    }
  }
  // quy định: nếu không tìm thấy thì trả về -1
  return -1;
}
function getIndexClassify(classify, staffArr) {
  for (var index = 0; index < staffArr.length; index++) {
    var item = staffArr[index];
    // nếu tìm thấy thì dừng function và trả về index hiện tại
    if (item.classify() == classify) {
      return index;
    }
  }
  // quy định: nếu không tìm thấy thì trả về -1
  return -1;
}

function reshowInfo(data) {
  document.getElementById("tknv").value = data.user;
  document.getElementById("name").value = data.name;
  document.getElementById("email").value = data.email;
  document.getElementById("password").value = data.pass;
  document.getElementById("datepicker").value = data.workDays;
  document.getElementById("luongCB").value = data.baseSal;
  document.getElementById("chucvu").value = data.position;
  document.getElementById("gioLam").value = data.workHours;
}
function resetForm() {
  document.getElementById("form_management").reset();
}
function showMessageErr(idErr, message, showErr) {
  document.getElementById(idErr).style.display = showErr;
  document.getElementById(idErr).innerHTML = message;
}

function resetErr() {
  document.getElementById("tbTKNV").style.display = "none";
  document.getElementById("tbTen").style.display = "none";
  document.getElementById("tbEmail").style.display = "none";
  document.getElementById("tbMatKhau").style.display = "none";
  document.getElementById("tbNgay").style.display = "none";
  document.getElementById("tbLuongCB").style.display = "none";
  document.getElementById("tbChucVu").style.display = "none";
  document.getElementById("tbGiolam").style.display = "none";
}

// nếu nhấn nút edit nhưng không chỉnh sửa và thoát ra thì khi nhấn lại nút thêm trường tài khoản sẽ bị khóa dòng code dưới đây để mở khóa
// nếu nhấn nút thêm nhập dữ liệu có lỗi nhưng không nhấn thêm thì dòng lỗi vẫn tồn tại khi nhấn bất kỳ nút nào nên phải resetErr
const themNhanVienBtn = document.getElementById("btnThem");
themNhanVienBtn.addEventListener("click", function () {
  document.getElementById("tknv").disabled = false;
  resetErr();
});
