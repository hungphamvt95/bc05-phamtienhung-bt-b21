function staffObj(
  user,
  name,
  email,
  passWord,
  workDays,
  baseSal,
  position,
  workHours,
) {
  this.user = user;
  this.name = name;
  this.email = email;
  this.pass = passWord;
  this.workDays = workDays;
  this.baseSal = baseSal;
  this.position = position;
  this.workHours = workHours;
  this.totalSalCal = function () {
    if (this.position == "Giám đốc") {
      return this.baseSal * 3;
    } else if (this.position == "Trưởng phòng") {
      return this.baseSal * 2;
    } else {
      return this.baseSal * 1;
    }
  };
  this.classify = function () {
    if (this.workHours >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.workHours >= 176) {
      return "Nhân viên giỏi";
    } else if (this.workHours >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}
